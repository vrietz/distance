/* Distance Tracker (JS) - Version 1.4
 * Tracks traveled distance of cursor or touch gestures.
 * Copyright (C) 2020  Vincent Rietz, Karina Renten
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

const diagonale_inch = getDiagonale();
const vertical_px = screen.height;
const horizontal_px = screen.width;

const factor = (1 / (Math.sqrt((horizontal_px * horizontal_px) + (vertical_px * vertical_px)))) * (diagonale_inch * 25.4);

let oldx = -1;
let oldy = -1;
let km = 0;
let m = 0;
let cm = 0;
let mm = 0;

document.addEventListener("mousemove", calculate, {passive: false});
document.addEventListener("mouseout", reset);

document.addEventListener("touchmove", calculate, {passive: false});
document.addEventListener("touchend", reset);
document.addEventListener("touchcancel", reset);

function calculate(e) {
    let x = 0;
    let y = 0;
    
    if (e.type == "mousemove") {
        x = e.clientX;
        y = e.clientY;
    } else if (e.type == "touchmove") {
        e.preventDefault();
    
        let touch = e.touches[0];
        
        x = touch.pageX;
        y = touch.pageY;
    } else {
        return;
    }
    
    if (x < 0 || y < 0 || x > document.documentElement.clientWidth || y > document.documentElement.clientHeight) {
        reset();
    } else {
        if (!(oldx == -1 && oldy == -1)) {
            let distance_px = Math.sqrt(((x - oldx) * (x - oldx)) + ((y - oldy) * (y - oldy)));
            let distance_mm = distance_px * factor;
            mm += distance_mm;
            
            let new_cm = Math.floor(mm / 10);
            cm += new_cm;
            mm -= (new_cm * 10);
            
            let new_m = Math.floor(cm / 100);
            m += new_m;
            cm -= (new_m * 100);
            
            let new_km = Math.floor(m / 1000);
            km += new_km;
            m -= (new_km * 1000);
        }

        oldx = x;
        oldy = y;
        
        document.getElementById("div_mm").innerHTML = Math.trunc(mm) + "&#8239;mm";
        document.getElementById("div_cm").innerHTML = Math.trunc(cm) + "&#8239;cm";
        document.getElementById("div_m").innerHTML = Math.trunc(m) + "&#8239;m";
        document.getElementById("div_km").innerHTML = Math.trunc(km) + "&#8239;km";
    }
}

function reset() {
    oldx = -1;
    oldy = -1;
}

function getDiagonale() {
    let d = new URLSearchParams(window.location.search).get('d');
    return (d != null && d > 0 && d < 19726) ? d : 15;
}

function showOverlay() {
    document.getElementById("overlay").style.display = "block";
}

function hideOverlay() {
    document.getElementById("overlay").style.display = "none";
}
