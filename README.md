# Distance Tracker
Small HTML/CSS/JavaScript webapp to measure and show the distance a user moves across the screen (with either mouse cursor or touch gestures).

## Usage
The project is available online under [vrietz.gitlab.io/distance/](https://vrietz.gitlab.io/distance/).

For correct functionality, set your display size for the project by adding `?d=[size in inches]` to the end of the URL.  
Example: `.../distance/?d=15`.

For offline use, download the project and open `public/index.html` in a browser.

## License 
The project is published under the GNU General Public License Version 3.
